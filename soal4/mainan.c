#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

int main(int argc, char *argv[]) {
    // Cek jumlah argumen yang diberikan
    if (argc != 5) {
        printf("error: jumlah argumen yang diberikan salah\n");
        return 1;
    }
    
    // Deklarasi variabel-variabel yang dibutuhkan
    int hour, minute, second;
    char *filename;
    struct tm *timeinfo;
    time_t rawtime;

    // Konversi argumen-argumen waktu menjadi integer
    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            printf("error: argumen jam tidak valid\n");
            return 1;
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            printf("error: argumen menit tidak valid\n");
            return 1;
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            printf("error: argumen detik tidak valid\n");
            return 1;
        }
    }

    // Simpan argumen path file .sh
    filename = argv[4];

    // Cek apakah file .sh yang ditunjuk oleh path tersedia dan bisa diakses
    if (access(filename, F_OK) == -1) {
        printf("error: file %s tidak tersedia atau tidak bisa diakses\n", filename);
        return 1;
    }

    // Main loop program
    while (1) {
        // Dapatkan waktu sekarang
        time(&rawtime);
        timeinfo = localtime(&rawtime);

        // Cek apakah waktu sekarang sesuai dengan argumen
        if ((hour == -1 || hour == timeinfo->tm_hour) &&
            (minute == -1 || minute == timeinfo->tm_min) &&
            (second == -1 || second == timeinfo->tm_sec)) {
            // Jika sesuai, jalankan file .sh
            if (fork() == 0) {
                execl("/bin/sh", "sh", filename, NULL);
                exit(0);
            }
        }

        // Tunggu 1 detik sebelum cek kembali waktu
        sleep(1);
    }

    return 0;
}
