# sisop-praktikum-modul-2-2023-MHFD-IT06



- Fathika Afrine Azaruddin - 5027211016
- Athallah Narda Wiyoga - 5027211041
- Gilbert Immanuel Hasiholan - 5027211056

# Soal no. 1
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
```
Beberapa direktif preprosesor yang digunakan untuk mengimpor header file yang diperlukan untuk operasi I/O, manipulasi string, pengelolaan file, dan zip.

```c
int zip(char *zipfilename, char *directory)
```
Deklarasi sebuah fungsi bernama zip yang menerima dua argumen string, yaitu zipfilename yang merupakan nama file zip yang akan dihasilkan, dan directory yang merupakan direktori yang akan dikompresi menjadi file zip.

```c
struct zip *za = zip_open(zipfilename, ZIP_CREATE, NULL);
if (!za)
{
    return 0;
}
```
Pemanggilan fungsi zip_open untuk membuka file zip yang akan dihasilkan. Fungsi ini mengembalikan pointer ke objek struct zip yang digunakan untuk operasi pengompresan. Lalu dilakukan pengujian apakah file zip berhasil dibuka. Jika tidak, maka fungsi akan mengembalikan nilai 0 sebagai indikasi bahwa operasi pengompresan gagal.

```c
DIR *d = opendir(directory);
if (!d)
{
    zip_close(za);
    return 0;
}
```
Pemanggilan fungsi opendir untuk membuka direktori yang akan dikompresi. Fungsi ini mengembalikan pointer ke objek DIR yang digunakan untuk operasi pada direktori. Lalu dilakukan pengujian apakah direktori berhasil dibuka. Jika tidak, maka fungsi akan menutup file zip yang telah dibuka sebelumnya dengan memanggil fungsi zip_close, dan mengembalikan nilai 0 sebagai indikasi bahwa operasi pengompresan gagal.

```c
struct dirent *dir;
while ((dir = readdir(d)) != NULL)
```
Penggunaan loop while untuk membaca isi dari direktori yang akan dikompresi. Setiap file atau direktori dalam direktori tersebut akan diproses dalam loop ini.

```c
const char *ext = strrchr(dir->d_name, '.');
if (ext && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0))
```
Penggunaan fungsi strrchr untuk mencari karakter titik (.) terakhir dalam nama file atau direktori yang sedang diproses, yang mengindikasikan ekstensi file. Lalu dilakukan pengujian apakah file atau direktori yang sedang diproses memiliki ekstensi .jpg, .jpeg, atau .png. Jika ya, maka file atau direktori tersebut akan dimasukkan ke dalam file zip yang akan dihasilkan.

```c
char filepath[256];
snprintf(filepath, sizeof(filepath), "%s/%s", directory, dir->d_name);
```
Deklarasi variabel lokal filepath sebagai array karakter dengan panjang 256, yang digunakan untuk menyimpan path file yang akan dimasukkan ke dalam file zip dan menggunakan fungsi snprintf() untuk menggabungkan path direktori (directory) dengan nama file (dir->d_name) yang sedang diiterasi, dan menyimpan hasilnya dalam variabel filepath. %s adalah spesifikasi format untuk string, dan %s/%s adalah string format yang digunakan untuk menggabungkan directory dan nama file.

```c
struct zip_source *source = zip_source_file(za, filepath, 0, 0);
if (!source)
{
    zip_close(za);
    closedir(d);
    return 0;
}
```
Membuka file yang akan dimasukkan ke dalam file zip menggunakan fungsi zip_source_file(), yang menghasilkan sebuah objek zip_source yang merupakan sumber data untuk dimasukkan ke dalam zip. za adalah objek zip yang digunakan untuk mengelola file zip, filepath adalah path file yang akan dimasukkan, dan 0 dan 0 adalah offset dan ukuran data yang ingin dimasukkan (dalam hal ini, dimulai dari awal file dan seluruh isi file). Lalu memeriksa apakah objek zip_source yang diperoleh dari fungsi zip_source_file() berhasil dibuat atau tidak (nilai NULL). Jika tidak, maka akan dilakukan penutupan objek zip dengan fungsi zip_close(), menutup direktori dengan fungsi closedir(), dan mengembalikan nilai 0 yang menandakan bahwa kompresi gagal.

```c
if (zip_file_add(za, dir->d_name, source, ZIP_FL_OVERWRITE) < 0)
{
    zip_source_free(source);
    zip_close(za);
    closedir(d);
    return 0;
}
```
Menambahkan file ke dalam file zip dengan menggunakan fungsi zip_file_add(). za adalah objek zip yang digunakan, dir->d_name adalah nama file yang akan digunakan dalam zip (dalam hal ini, nama file asli), source adalah objek zip_source yang menjadi sumber data file yang akan dimasukkan, dan ZIP_FL_OVERWRITE adalah flag yang menandakan bahwa jika file dengan nama yang sama sudah ada dalam zip, maka akan ditimpa (overwrite). Jika nilai yang dikembalikan oleh fungsi zip_file_add() kurang dari 0, artinya ada kesalahan dalam menambahkan file ke dalam zip, maka akan dilakukan pembebasan memori objek zip_source dengan fungsi zip_source_free(), penutupan objek zip dengan fungsi zip_close(), penutupan direktori dengan fungsi closedir(), dan mengembalikan nilai 0 yang menandakan bahwa kompresi gagal.

```c
closedir(d);
return zip_close(za) == 0 ? 1 : 0;
```
Menutup direktori setelah selesai mengiterasi file-file di dalamnya. Lalu menutup objek zip.

```c
system("wget \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\" -O images.zip");
```
Fungsi system() digunakan untuk menjalankan perintah shell pada sistem operasi. Di sini, perintah wget digunakan untuk mengunduh file zip dari URL tertentu dan menyimpannya sebagai images.zip.

```c
const char *filename = "images.zip";
struct zip *zip_file = zip_open(filename, 0, NULL);
```
Fungsi zip_open() dari pustaka libzip digunakan untuk membuka file zip dengan nama file "images.zip" dan mengembalikan pointer ke objek zip_file yang digunakan untuk mengakses isi file zip.

```c
int num_entries = zip_get_num_entries(zip_file, 0);
for (int i = 0; i < num_entries; i++)
```
Fungsi zip_get_num_entries() digunakan untuk mengambil jumlah entri/file dalam file zip. Selanjutnya, loop for digunakan untuk mengiterasi setiap entri dalam file zip.

```c
struct zip_stat file_stat;
if (zip_stat_index(zip_file, i, 0, &file_stat) != 0)
```
Fungsi zip_stat_index() digunakan untuk mengambil informasi (statistik) mengenai entri/file dalam file zip. Informasi ini disimpan dalam objek file_stat yang merupakan objek dari struktur zip_stat.

```c
char output_file[256];
strcpy(output_file, file_stat.name);

if (output_file[strlen(output_file) - 1] == '/')
{
    continue;
}
```
Struktur zip_stat memiliki atribut name yang berisi nama entri/file dalam file zip. Jika entri tersebut adalah direktori (diketahui dari karakter '/' pada akhir nama file), maka entri tersebut diabaikan dan iterasi dilanjutkan ke entri berikutnya.

```c
struct zip_file *file = zip_fopen_index(zip_file, i, 0);
if (!file)
```
Fungsi zip_fopen_index() digunakan untuk membuka file dalam file zip berdasarkan indeks entri. Fungsi ini mengembalikan pointer ke objek zip_file yang digunakan untuk membaca isi file dalam entri.

```c
FILE *fp = fopen(output_file, "wb");
if (!fp)
{
    fprintf(stderr, "Error creating output file.\n");
    zip_fclose(file);
    zip_close(zip_file);
    return 1;
}
```
Program membuka sebuah file dengan mode "wb" (binary write) menggunakan fopen() dan menyimpan file pointer ke dalam variabel fp. Jika pembukaan file gagal, program mencetak pesan error ke stderr menggunakan fprintf() dan kemudian menutup file zip dan mengembalikan nilai 1 sebagai kode error.

```c
char buffer[4096];
int n;
do
{
    n = zip_fread(file, buffer, sizeof(buffer));
    if (n > 0)
    {
        fwrite(buffer, n, 1, fp);
    }
} while (n > 0);

fclose(fp);
zip_fclose(file);
```
Program membaca isi file zip menggunakan zip_fread() yang mengambil data dari file zip dan menyimpannya ke dalam buffer buffer sebesar sizeof(buffer). Jika ada data yang berhasil dibaca, program menuliskan data tersebut ke dalam file yang telah dibuka sebelumnya menggunakan fwrite(). Proses membaca dan menulis dilakukan dalam loop do-while sampai tidak ada data lagi yang bisa dibaca. Setelah selesai membaca dan menulis file zip, program menutup file menggunakan fclose() dan zip_fclose(). Kemudian, program menutup zip file menggunakan zip_close().

```c
const char *valid_extensions[] = {".jpg", ".jpeg"};
int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);
```
Program mendefinisikan ekstensi file yang diizinkan dalam array valid_extensions dimana file yang diizinkan adalah jpg dan jpeg. Lalu int num_valid_extensions untuk menghitung jumlah elemen dalam array tersebut untuk digunakan nanti.

```c
DIR *dir = opendir(".");
if (dir == NULL) {
    fprintf(stderr, "Error\n");
    return 1;
}

const int MAX_FILES = 1000;
char filenames[MAX_FILES][256];
int num_files = 0;
struct dirent *entry;
while ((entry = readdir(dir)) != NULL) {
    char *ext = strrchr(entry->d_name, '.');
    if (ext == NULL) {
        continue;
    }
    for (int i = 0; i < num_valid_extensions; i++) {
        if (strcmp(ext, valid_extensions[i]) == 0) {
            strcpy(filenames[num_files], entry->d_name);
            num_files++;
            break;
        }
    }
}
closedir(dir);
```
Program kemudian membuka direktori saat ini menggunakan opendir(".") dan membaca setiap file dalam direktori tersebut menggunakan loop while dan readdir(). Untuk setiap file yang ditemukan, program memeriksa apakah file tersebut memiliki ekstensi yang diizinkan (.jpg atau .jpeg) menggunakan strrchr() untuk mengecek ekstensi file berdasarkan tanda titik terakhir dalam nama file. Jika file tersebut memiliki ekstensi yang diizinkan, nama file tersebut disalin ke dalam array filenames menggunakan strcpy(). Jumlah file yang ditemukan juga dihitung. Setelah selesai membaca direktori, program menutup direktori menggunakan closedir().

```c
srand(time(NULL));
int random_index = rand() % num_files;
char *random_filename = filenames[random_index];
printf("Shift Penjagaan: %s\n", random_filename);

printf("File berhasil di-un zip.\n");
```
Program menggunakan srand(time(NULL)) untuk menginisialisasi seed untuk fungsi rand() berdasarkan waktu saat ini, dan kemudian menggunakan rand() % num_files untuk menghasilkan indeks acak dari array filenames. Nama file yang dipilih secara acak akan dicetak ke stdout menggunakan printf() dalam format "Shift Penjagaan: %s\n". Apabila semua proses yang telah dilalui berhasil maka akan memunculkan tulisan "File berhasil di-un zip.\n".

```c
system("mkdir -p HewanDarat");
system("mkdir -p HewanAmphibi");
system("mkdir -p HewanAir");
```
Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir menggunakan perintah sistem mkdir -p, di mana -p digunakan untuk membuat direktori secara rekursif jika belum ada.

```c
system("mv *_darat.jpg HewanDarat");
system("mv *_amphibi.jpg HewanAmphibi");
system("mv *_air.jpg HewanAir");
```
Memindahkan file gambar dengan ekstensi _darat.jpg, _amphibi.jpg, dan _air.jpg ke direktori yang sesuai menggunakan perintah sistem mv (move).

```c
zip("HewanDarat.zip", "HewanDarat");
zip("HewanAmphibi.zip", "HewanAmphibi");
zip("HewanAir.zip", "HewanAir");
```
Mengompresi file di dalam direktori HewanDarat, HewanAmphibi, dan HewanAir menjadi file zip masing-masing menggunakan fungsi zip() (asumsi fungsi zip() telah didefinisikan sebelumnya dalam kode yang tidak terlihat dalam contoh yang diberikan).

```c
printf("Pemindahan file gambar berhasil dilakukan.\n");
```
Mencetak pesan "Pemindahan file gambar berhasil dilakukan.\n" ke stdout menggunakan printf().

Hasil Output: 
https://ibb.co/0rGPBg1

# Soal no. 2

```c
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
```
Baris di atas mendefinisikan beberapa header file yang dibutuhkan untuk menjalankan program ini

```c
void removeDir(char *folder_name);
void zip(char *folder_name);
void terminateProgram(char *program_name, int mode, int pid);
void downloadImage(char *folder_name);
void createFolder();
void handleArguments(int argc, char *argv[]);
```
Bagian ini merupakan pendefinisian fungsi yang akan digunakan dalam program ini.

```c
int main(int argc, char *argv[])
{
    char opt = argv[1][1];
    int mode;
     switch (opt)
        {
        case 'a':
            mode = 1;
            break;
        case 'b':
            mode = 2;
            break;
        }
```
Baris ini merupakan fungsi utama dari program dimana bagian ini akan mengekstrak argumen yang diberikan pada program dan menentukan mode yang akan digunakan. Mode 1 digunakan jika argumen yang diberikan adalah "-a" dan mode 2 digunakan jika argumen yang diberikan adalah "-b".

```c
    pid_t pid = fork();
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        if (mode == 1)
        {
            terminateProgram(argv[0], mode, 0);
        }
        exit(EXIT_SUCCESS);
    }
```
Pada bagian ini, program melakukan proses fork. Jika proses fork gagal maka program akan keluar dengan status EXIT_FAILURE. Jika proses fork berhasil maka program akan melakukan pengecekan apakah mode yang digunakan adalah mode 1. Jika ya, maka program akan memanggil fungsi terminateProgram dan keluar dengan status EXIT_SUCCESS.

```c
    umask(0);
    pid_t sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```
Bagian ini akan membuat program menjadi daemon dengan menggunakan fungsi setsid. Pada bagian ini juga, program menutup stdin, stdout, dan stderr untuk mencegah program menuliskan output ke terminal.

```c
    terminateProgram(argv[0], mode, (int)getpid());
    while (1)
    {
        pid_t child_pid = fork();
        if (child_pid < 0)
        {
            perror("Error creating child process");
            exit(EXIT_FAILURE);
        }
        else if (child_pid == 0)
        {
            createFolder();
            exit(EXIT_SUCCESS);
        }
        sleep(30);
    }
}
```
Pada bagian ini, program akan memanggil fungsi terminateProgram untuk membuat file "killer". Setelah itu, program akan masuk ke dalam loop yang akan terus berjalan selama program berjalan. Pada setiap iterasinya, program akan melakukan proses fork dan memanggil fungsi createFolder. Setelah itu, program akan tidur selama 30 detik sebelum melakukan iterasi berikutnya.

```c
void removeDir(char *folder_name)
{
    char *argv[] = {"rm", "-rf", folder_name, NULL};
    execv("/bin/rm", argv);
}
```
Fungsi removeDir berfungsi untuk menghapus direktori. Dalam fungsi ini, program menginisialisasi array "argv" yang berisi argumen-argumen yang akan di-pass ke program "rm" melalui "execv". "folder_name" adalah argumen pertama yang diberikan dan "-rf" adalah opsi kedua. Kemudian, fungsi "execv" digunakan untuk menjalankan program "rm" dengan argumen "argv" untuk menghapus direktori.

```c
void zip(char *folder_name)
{
    char zip_name[50];
    char cwd[1024];
    char path[100];
```
Baris di atas mendeklarasikan tiga variabel bertipe 'char'. 'zip_name' akan digunakan untuk menyimpan nama file zip, 'cwd' akan digunakan untuk menyimpan direktori kerja saat ini, dan 'path' akan digunakan untuk menyimpan path ke folder yang akan di-zip.

```c
    getcwd(cwd, sizeof(cwd));
    sprintf(zip_name, "%s.zip", folder_name);
    sprintf(path, "%s/%s/", cwd, folder_name);
```
Kemudian, pada baris di atas, fungsi 'getcwd' digunakan untuk mendapatkan direktori kerja saat ini dan menyimpannya pada variabel 'cwd'. Setelah itu, variabel 'zip_name' diisi dengan string yang terdiri dari nama folder yang diberikan sebagai argumen, diikuti dengan '.zip'. Variabel path diisi dengan string yang terdiri dari direktori kerja saat ini, diikuti dengan '/', diikuti dengan nama folder yang diberikan sebagai argumen, diikuti dengan '/'.

```c
    char *argv[] = {"zip", "-r", zip_name, path, NULL};
    pid_t child_zip, child_rmdir;
    child_zip = fork();
    int status;
```
Kemudian, pada baris di atas, sebuah array argv dibuat untuk menyimpan argumen-argumen yang akan diberikan pada fungsi execvp. Array ini terdiri dari string-string 'zip', '-r', 'zip_name', 'path', dan 'NULL'. Selain itu, variabel 'child_zip' dan 'child_rmdir' dideklarasikan bertipe 'pid_t' untuk menyimpan ID proses yang akan dibuat nanti. Variabel status dideklarasikan bertipe int untuk menyimpan status keluaran dari proses.

```c
    if (child_zip == 0)
    {
        execvp("zip", argv);
    }
    else
    {
        waitpid(child_zip, &status, 0);
        if (WIFEXITED(status))
        {
            child_rmdir = fork();
            if (child_rmdir == 0)
            {
                removeDir(folder_name);
            }
        }
    }
}
```
Kemudian, pada blok kode di atas, dilakukan pengecekan apakah proses yang dijalankan adalah proses child atau bukan dengan memeriksa nilai dari variabel 'child_zip'. Jika 'child_zip' sama dengan 0, maka kode di dalam blok if akan dieksekusi, yaitu menjalankan fungsi 'execvp' untuk melakukan zip pada folder yang telah ditentukan sebelumnya menggunakan argumen-argumen yang telah didefinisikan sebelumnya.

Namun, jika proses yang dijalankan bukan proses child, maka blok else akan dieksekusi. Pada blok else, fungsi waitpid digunakan untuk menunggu proses child selesai dieksekusi. Setelah proses child selesai dieksekusi, program akan mengecek apakah proses tersebut berakhir dengan sukses menggunakan WIFEXITED. Jika proses child berhasil dieksekusi, maka program akan membuat proses baru untuk menghapus direktori yang telah di-zip menggunakan fungsi removeDir().

```c
void terminateProgram(char *program_name, int mode, int pid)
{
    FILE *killer_file = fopen("killer", "w");
```
Selanjutnya adalah fungsi terminateProgram, yang dimana fungsi ini memiliki tiga argumen: program_name, mode, dan pid. Program_name berisi nama program yang akan di-terminate, mode berisi mode terminator, dan pid berisi id proses yang akan di-terminate.

Pada baris pertama di atas, fungsi membuka file baru dengan nama "killer" menggunakan fungsi fopen. File tersebut akan dihasilkan sebagai file bash sederhana yang berisi perintah untuk men-terminate program yang sedang berjalan.

```c
    if (mode == 1)
    {
        fprintf(killer_file, "#!/bin/bash\nkill %d\nrm killer\n", pid);
    }
    else
    {
        fprintf(killer_file, "#!/bin/bash\npkill -f \"%s -b\"\nrm killer\n", program_name);
    }
```
Kemudian, pada blok if-else di atas, akan di-generate perintah bash yang berbeda tergantung pada nilai mode. Jika mode sama dengan 1, maka fungsi fprintf akan menulis perintah "kill" pada file "killer", yang akan men-terminate proses dengan ID yang telah disediakan. Jika mode tidak sama dengan 1, maka fungsi fprintf akan menulis perintah "pkill" pada file "killer", yang akan men-terminate proses dengan nama program yang telah disediakan.

```c
    fclose(killer_file);
    chmod("killer", 0700);
}
```
Pada akhirnya, fungsi fclose digunakan untuk menutup file "killer", dan kemudian fungsi chmod digunakan untuk mengubah permission dari file "killer" agar dapat dijalankan dengan perintah bash. Permission tersebut diatur menggunakan mode 0700, yang memungkinkan user yang membuat file (owner) untuk menjalankannya.

```c
void downloadImage(char *folder_name)
{
    for (int i = 0; i < 15; i++)
    {
        char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
        pid_t pid;
        time_t t, current_time;
        time(&t);
        struct tm *local = localtime(&t);
        strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
        sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat);
        current_time = time(NULL);
        current_time = (current_time % 1000) + 50;
        sprintf(str, "%ld", current_time);
        strcat(URL, str);
        char *argv[] = {"wget", "-O", fileFormat, URL, NULL};
        pid = fork();
        if (pid == 0)
        {
            execvp("wget", argv);
        }
        sleep(5);
    }
```
Lalu sekarang merupakan fungsi downloadImage, yang dimana pada bagian ini, dilakukan perulangan sebanyak 15 kali untuk mengunduh gambar. Setiap kali melakukan pengunduhan, dibuat variabel-variabel yang digunakan untuk membentuk URL dari gambar yang akan diunduh, nama file hasil unduhan, dan argumen-argumen yang akan digunakan untuk menjalankan perintah wget.

Kemudian, dilakukan fork() untuk membuat proses child yang akan mengeksekusi perintah wget menggunakan fungsi execvp. Proses parent akan menunggu selama 5 detik sebelum melakukan pengunduhan gambar selanjutnya.

Setelah perulangan selesai, dilakukan fork() untuk membuat proses child yang akan memanggil fungsi zip untuk mengkompres gambar-gambar yang sudah diunduh menjadi satu file .zip. Proses parent kemudian keluar dari program.

```c
    pid_t child_zip;
    child_zip = fork();
    if (child_zip == 0)
    {
        zip(folder_name);
    }
    exit(EXIT_SUCCESS);
}
```
Pada bagian ini, dilakukan fork() untuk membuat proses child yang akan mengeksekusi fungsi zip. Proses parent kemudian keluar dari program dengan menggunakan fungsi exit().

```c
void createFolder()
{
    time_t t;
    time(&t);
    struct tm *local = localtime(&t);
    char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
    strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
```
Lalu fungsi terakhir adalah fungsi createFolder yang digunakan untuk membuat direktori baru dengan nama yang berdasarkan waktu saat ini, dan kemudian memanggil fungsi downloadImage() untuk mengunduh gambar dari internet ke dalam direktori tersebut. Pada bagian ini, waktu saat ini diambil menggunakan fungsi time(), lalu diubah ke dalam bentuk yang lebih mudah dibaca manusia menggunakan localtime(). Format waktu diatur menggunakan strftime() dan disimpan ke dalam variabel timeFormat.

```c
    char *argv[] = {"mkdir", "-p", timeFormat, NULL};
    if(fork() == 0) execv("/bin/mkdir", argv);
```
Pada bagian ini, direktori baru dengan nama yang sudah diatur sebelumnya dibuat menggunakan perintah mkdir. Argumen -p digunakan agar mkdir membuat direktori secara rekursif jika direktori induk belum ada. Fungsi fork() digunakan untuk membuat proses child yang akan menjalankan perintah mkdir menggunakan execv().

```c
    downloadImage(timeFormat);
}
```
Terakhir, pada bagian ini, fungsi downloadImage() dipanggil dengan argumen nama direktori yang baru saja dibuat. Fungsi ini akan mengunduh gambar dari internet dan menyimpannya ke dalam direktori yang sudah dibuat.

Hasil Output:
https://ibb.co/Sc5PjKH

# Soal no. 3

```c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>

```
Baris ini adalah file header yang diperlukan untuk mengeksekusi kode.

```c
#define MANUTD_TEAM "ManUtd"
#define MAX_FILENAME 256
```
Baris ini merupakan konstanta yang akan digunakan pada program ini.

```c
void downloadPlayers();
void extractPlayers();
void filterPlayers();
void categorizePlayers();
void buatTim(int bek, int gelandang, int penyerang);
```
Bagian ini merupakan pendefinisian fungsi yang akan digunakan dalam program ini.

```c
int main() {
    downloadPlayers();
    extractPlayers();
    filterPlayers();
    categorizePlayers();
    
    int bek, gelandang, penyerang;
    printf("Masukkan jumlah bek: ");
    scanf("%d", &bek);
    printf("Masukkan jumlah gelandang: ");
    scanf("%d", &gelandang);
    printf("Masukkan jumlah penyerang: ");
    scanf("%d", &striker);
  
    buatTim(bek, gelandang, penyerang);
    return 0;
}
```
Bagian ini merupakan fungsi utama yang memanggil fungsi sebelumnya secara berurutan. Kemudian meminta pengguna untuk memasukkan jumlah bek, gelandang, dan penyerang yang akan dipilih untuk tim sepak bola. Ini memanggil fungsi buatTim() dengan jumlah bek, gelandang, dan striker sebagai parameter.

```c
void downloadPlayers() {
    char* args[] = {"curl", "-L", "-o", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
```
Mendefinisikan array of string args yang berisi command-line arguments yang akan digunakan oleh curl. Disini, curl akan mengunduh file dari url diatas dan menyimpannya dengan nama players.zip.

```c
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
```
Mendefinisikan sebuah variable pid_t bernama pid untuk menampung PID (Process ID) dari child process. Kemudian, fork() dipanggil untuk membuat sebuah child process. Jika fork() mengembalikan nilai -1, maka ada error dalam pembuatan child process. Error tersebut akan dicetak menggunakan perror() dan program akan keluar dengan nilai EXIT_FAILURE.


```c
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);

```
Jika fork() mengembalikan nilai 0, maka program sedang berjalan pada child process. Child process akan menjalankan command-line tool curl dengan menggunakan execvp(). Jika ada error dalam menjalankan execvp(), error tersebut akan dicetak menggunakan perror() dan program akan keluar dengan nilai EXIT_FAILURE.

```c
    } else {
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error downloading players\n");
            exit(EXIT_FAILURE);
        }
    }
}
```
Jika fork() mengembalikan nilai selain -1 dan 0, maka program sedang berjalan pada parent process. Parent process akan menunggu child process selesai dijalankan menggunakan waitpid(). Jika status exit code dari child process bukan 0, maka ada error dalam mengunduh file, pesan error akan dicetak menggunakan printf() dan program akan keluar dengan nilai EXIT_FAILURE.

```c
void extractPlayers() {
    char* args[] = {"unzip", "players.zip", NULL};
```
Mendefinisikan fungsi extractPlayers() tanpa parameter dan dengan tipe kembalian void. Kemudian mendefinisikan array args dengan isi "unzip" dan "players.zip". Array ini akan digunakan sebagai argumen untuk perintah execvp() untuk mengekstrak file players.zip.

```c
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
```
Bagian ini membuat proses baru menggunakan fork(), dan menyimpan nilai pengembalian ke dalam variabel pid. Jika terjadi kesalahan pada saat membuat proses baru, maka program akan mencetak pesan kesalahan dan keluar dengan status EXIT_FAILURE.

```c
    } else if (pid == 0) {
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
```
Jika pid sama dengan 0, maka ini merupakan child process dimana child process ini akan menjalankan perintah execvp() untuk mengekstrak file players.zip dengan menggunakan perintah unzip. Jika terjadi kesalahan pada saat menjalankan perintah execvp(), maka program akan mencetak pesan kesalahan dan keluar dengan status EXIT_FAILURE.

```c
    if (remove("players.zip") != 0) {
        printf("Error deleting file: players.zip\n");
    }
}
```
Baris ini menghapus file players.zip setelah selesai diekstrak. Jika terjadi kesalahan pada saat menghapus file, maka program akan mencetak pesan kesalahan.

```c
void filterPlayers() {
    DIR* dir = opendir("players");
    struct dirent* entry;
    char fileName[MAX_FILENAME];
```
Bagian ini membuka direktori "players" dan menyimpan file di dalam variabel dir. Kemudian, entry digunakan untuk menyimpan informasi tentang entri saat ini, dan fileName digunakan untuk menyimpan nama file yang akan dihapus.

```c
    if (dir == NULL) {
        printf("Error opening directory\n");
        exit(EXIT_FAILURE);
    }
```
Memeriksa apakah direktori "players" berhasil dibuka. Jika tidak, maka program akan mencetak pesan kesalahan dan keluar dari program.

```c
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) {
            if (fnmatch("*_ManUtd_*_*.png", entry->d_name, 0) != 0) {
                sprintf(fileName, "%s/%s", "players", entry->d_name);
                if (unlink(fileName) != 0) {
                    printf("Error deleting file: %s\n", fileName);
                }
            }
        }
    }
    closedir(dir);
}
```
Mengecek setiap entri di direktori "players" dan memeriksa apakah entri tersebut adalah file biasa. Jika entri adalah file biasa dan tidak sesuai dengan pola "ManUtd_*.png", maka file tersebut akan dihapus menggunakan fungsi unlink(). Jika penghapusan gagal, maka program akan mencetak pesan kesalahan. Kemudian menutup direktori setelah selesai menghapus file yang tidak sesuai dengan pola.


```c
void categorizePlayers() {
    DIR * dir = opendir("./players");
```
Membuka direktori "./players" dan menyimpan pointer direktori ke dalam variabel dir.

```c
    struct dirent * entry;
    char * image_file;
    char * positions[] = {
      "Kiper",
      "Bek",
      "Gelandang",
      "Penyerang"
    };
```
Deklarasi variabel entri bertipe struct dirent yang akan digunakan untuk menyimpan informasi tentang setiap entri dalam direktori. Variabel image_file akan digunakan untuk menyimpan nama file gambar pemain. Variabel positions adalah array string yang berisi daftar posisi pemain yang akan digunakan untuk membuat direktori baru.

```c
    if (dir == NULL) {
      printf("Error opening directory\n");
      exit(EXIT_FAILURE);
    }
```
Jika terjadi kesalahan saat membuka direktori, program akan menampilkan pesan error dan keluar dari program dengan exit code EXIT_FAILURE.

```c
    int i;
    for (i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) {
        char dir_name[MAX_FILENAME];
        sprintf(dir_name, "./%s", positions[i]);

        if (access(dir_name, F_OK) != 0) {
            char *args[] = {
                "mkdir",
                dir_name,
                NULL
            };
            pid_t pid = fork();

            if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
            } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
            } else {
                int status;
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                    printf("Error creating directory: %s\n", dir_name);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    rewinddir(dir);
```
Untuk setiap posisi pemain dalam array positions, program akan membuat direktori baru dengan nama posisi pemain tersebut. Pertama, program akan menggunakan fungsi access() untuk mengecek apakah direktori dengan nama tersebut sudah ada. Jika belum ada, program akan membuat direktori baru dengan menggunakan fungsi fork() dan execvp(). Setelah proses selesai, program akan mengecek status proses dan menampilkan pesan error jika proses gagal. Kemudian me-reset posisi aliran direktori yang dirujuk ke awal direktori.

```c
    while ((entry = readdir(dir)) != NULL) {
```
Baris di atas akan mengambil setiap entri di dalam direktori players satu per satu dan menjalankan kode di dalam blok while jika masih ada entri tersisa. readdir() akan mengembalikan pointer ke struktur yang berisi informasi tentang entri file.

```c
      if (entry -> d_type == DT_REG) {
        image_file = entry -> d_name;
        char * ext = strrchr(image_file, '.');
```
Baris di atas akan memeriksa apakah entri saat ini adalah file biasa. Kemudian akan menyimpan nama file ke dalam variabel image_file. Lalu baris terakhir akan mencari tanda titik pada nama file yang menandakan ekstensi file. Jika ditemukan, maka ext akan menyimpan pointer ke karakter setelah titik terakhir dalam nama file.

```c
        if (ext != NULL && (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".png") == 0)) {
```
Baris ini memeriksa apakah ekstensi file yang ditemukan adalah ".jpeg", ".jpg", atau ".png" dengan memeriksa apakah pointer ext yang berisi ekstensi file tidak null dan apakah ekstensi tersebut sama dengan salah satu dari tiga nilai yang diijinkan menggunakan fungsi strcmp.

```c
          int i;
          for (i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) {
            if (strstr(image_file, positions[i]) != NULL) {
```
Baris tersebut akan mencari posisi pemain dengan memeriksa apakah nama file mengandung nama posisi pemain yang terdapat dalam array positions.

```c
              char new_path[MAX_FILENAME];
              sprintf(new_path, "./%s/%s", positions[i], image_file);

              char old_path[MAX_FILENAME];
              sprintf(old_path, "./players/%s", image_file);
```
Baris ini akan menyiapkan path baru dan path lama untuk memindahkan file. Path baru adalah path menuju direktori posisi pemain, sementara path lama adalah path menuju direktori players.

```c
              char * args[] = {
                "mv",
                old_path,
                new_path,
                NULL
              };
              pid_t pid = fork();
```
Variabel args akan menyimpan command untuk memindahkan file dari lokasi awal ke lokasi tujuan menggunakan perintah mv pada terminal. Variabel pid akan menyimpan nilai dari fork() untuk mengecek apakah proses berjalan pada parent atau child process.

```c
              if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
              } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
              } else {
                int status;
                waitpid(pid, & status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                  printf("Error moving file: %s\n", image_file);
                  exit(EXIT_FAILURE);
                }
              }
              break;
            }
          }
        }
      }
    closedir(dir);
```
Jika fork() berhasil, maka proses pemindahan file akan dilakukan. Jika proses berjalan pada child process, maka perintah untuk memindahkan file akan dijalankan menggunakan execvp(). Jika proses berjalan pada parent process, maka parent process akan menunggu child process selesai dan mengecek apakah proses pemindahan file berhasil atau gagal. Setelah proses kategorisasi selesai dilakukan, perulangan akan dihentikan. Kemudian direktori yang dibuka dengan opendir() ditutup dengan menggunakan closedir().

```c
    char * args[] = {
      "rm",
      "-r",
      "./players",
      NULL
    };
```
Array char * yang berisi argumen-argumen untuk command rm untuk menghapus direktori ./players dibuat.

```c
    pid_t pid = fork();

    if (pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
    } else if (pid == 0) {
      execvp(args[0], args);
      perror("execvp");
      exit(EXIT_FAILURE);
    }
```
Sebuah proses baru dibuat dengan fork(), dan child process menjalankan command rm dengan menggunakan execvp(). Jika execvp() mengembalikan nilai -1, artinya terjadi error dan program keluar dari child process dengan memanggil exit(EXIT_FAILURE).

```c
    } else {
      int status;
      waitpid(pid, & status, 0);
      if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        printf("Error removing directory: %s\n", "./players");
        exit(EXIT_FAILURE);
      }
    }
}
```
Jika child process berhasil menjalankan command rm, parent process menunggu child process selesai dengan menggunakan waitpid(). Jika child process keluar dengan status 0, artinya command rm berhasil dijalankan dan direktori ./players telah terhapus. Jika status yang diterima bukan 0, maka program keluar dari parent process dengan memanggil exit(EXIT_FAILURE).

- https://ibb.co/1KrGnb1 (Hasilnya bila dilihat dari direktori keseluruhan)
- https://ibb.co/MnszGsj (Isi direktori Kiper)
- https://ibb.co/Qf3YgVk (Isi direktori Bek)
- https://ibb.co/m9Fp7QN (Isi direktori Gelandang)
- https://ibb.co/m4T1Vrz (Isi direktori Penyerang)

```c
void buatTim(int bek, int gelandang, int penyerang) {
  char * positions[] = {
    "Kiper",
    "Bek",
    "Gelandang",
    "Penyerang"
  };
  char * formasi = malloc(sizeof(char) * MAX_FILENAME);
  // sprintf(formasi, "/home/kali/Formasi_%d-%d-%d.txt", bek, gelandang, striker); // Kalo mau naro di home/[users]
  sprintf(formasi, "Formasi_%d-%d-%d.txt", bek, gelandang, striker);
  FILE * teamFile = fopen(formasi, "w");
  if (teamFile == NULL) {
    printf("Error creating team file: %s\n", strerror(errno));
    return;
  }
  fprintf(teamFile, "Formasi %d-%d-%d\n\n", bek, gelandang, penyerang);
```
Fungsi buatTim dimulai dengan membuat array positions yang berisi nama-nama posisi pemain (kiper, bek, gelandang, penyerang). Kemudian, dilakukan pembuatan sebuah string formasi dengan menggunakan sprintf(), yang akan menjadi nama file untuk menulis formasi tim dengan format nama Formasi_[Jumlah Bek]-[Jumlah Gelandang]-[Jumlah Penyerang].txt. Setelah itu, kita membuka file tersebut dengan fopen() menggunakan mode "w" untuk menulis. Jika tidak dapat membuka file tersebut, akan muncul pesan error. Terakhir, program menulis formasi ke dalam file dengan fprintf().

```c
  int ratings[5][20] = {
    0
  };
  char playerNames[5][20][MAX_FILENAME];
  int count[5] = {
    0
  };

  for (int i = 0; i < 4; i++) {
    DIR * dir = opendir(positions[i]);
    if (dir == NULL) {
      printf("Error opening directory %s\n", positions[i]);
      continue;
    }
    struct dirent * entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) {
        sprintf(fileName, "%s/%s", positions[i], entry -> d_name);
        char * rating = strrchr(entry -> d_name, '_') + 1;
        char * name = entry -> d_name;
        name[strlen(name) - strlen(rating) - 1] = '\0';
        int ratingVal = atoi(rating);
        ratings[i + 1][count[i + 1]] = ratingVal;
        strcpy(playerNames[i + 1][count[i + 1]], name);
        count[i + 1]++;
      }
    }
    closedir(dir);
  }
```
Bagian ini akan membuka setiap folder posisi pemain menggunakan loop for pada i dengan nilai awal 0 dan batas 4 (posisi kiper, bek, gelandang, dan penyerang). Kemudian, setiap folder dibuka menggunakan opendir() dan ditentukan apakah berhasil dibuka atau tidak. Jika tidak berhasil, akan dicetak pesan kesalahan. Jika berhasil dibuka, setiap file di dalam folder akan dibaca menggunakan readdir(). Jika file tersebut bukan direktori (tipe DT_REG), maka rating pemain diambil dari nama file menggunakan strrchr(), dan nama pemain diambil dari nama file dan dihapus ratingnya menggunakan strcpy(). Kemudian, rating pemain dan nama pemain dimasukkan ke array ratings dan playerNames, dan variabel count untuk posisi yang bersangkutan akan ditingkatkan.

```c
  int bestKiper = 0;
  int bestBek[bek];
  int bestGelandang[gelandang];
  int bestPenyerang[penyerang];

  for (int i = 0; i < count[1]; i++) {
    if (ratings[1][i] > ratings[1][bestKiper]) {
      bestKiper = i;
    }
  }
```
Pada bagian ini, ditetapkan variabel bestKiper, bestBek, bestGelandang, dan bestPenyerang. Variabel bestKiper akan menyimpan indeks pemain dengan rating tertinggi untuk posisi kiper. Variabel bestBek, bestGelandang, dan bestPenyerang akan menyimpan indeks pemain dengan rating tertinggi untuk masing-masing posisi.

Kemudian, program melakukan loop sebanyak count[1], yaitu jumlah pemain yang berposisi sebagai kiper. Jika rating dari pemain tersebut lebih besar dari rating pemain yang saat ini dianggap sebagai pemain terbaik untuk posisi kiper (bestKiper), maka variabel bestKiper akan diupdate dengan indeks pemain tersebut.

```c
  for (int i = 0; i < bek; i++) {
    bestBek[i] = -1;
  }

  for (int i = 0; i < count[2]; i++) {
    for (int j = 0; j < bek; j++) {
      if (bestBek[j] == -1 || ratings[2][i] > ratings[2][bestBek[j]]) {
        int k;
        for (k = bek - 1; k > j; k--) {
          bestBek[k] = bestBek[k - 1];
        }
        bestBek[j] = i;
        break;
      }
    }
  }
```
Pada bagian ini, program mengisi semua elemen pada array bestBek dengan nilai -1, menunjukkan bahwa belum ada pemain terbaik untuk posisi bek. Kemudian, dilakukan loop sebanyak count[2], yaitu jumlah pemain yang berposisi sebagai bek. Di dalam loop tersebut, dibandingkan rating pemain dengan rating pemain terbaik yang saat ini disimpan di bestBek. Jika rating pemain tersebut lebih besar dari rating pemain terbaik yang saat ini disimpan di bestBek, maka program akan menggeser semua pemain pada posisi bestBek[j] sampai bestBek[bek-2] ke kanan satu posisi untuk memberi tempat pada pemain dengan rating yang lebih tinggi. Kemudian, pemain baru tersebut akan diisi ke dalam bestBek[j].

```c
  for (int i = 0; i < gelandang; i++) {
    bestGelandang[i] = -1;
  }

  for (int i = 0; i < count[3]; i++) {
    for (int j = 0; j < gelandang; j++) {
      if (bestGelandang[j] == -1 || ratings[3][i] > ratings[3][bestGelandang[j]]) {
        int k;
        for (k = gelandang - 1; k > j; k--) {
          bestGelandang[k] = bestGelandang[k - 1];
        }
        bestGelandang[j] = i;
        break;
      }
    }
  }

  for (int i = 0; i < striker; i++) {
    bestPenyerang[i] = -1;
  }

  for (int i = 0; i < count[4]; i++) {
    for (int j = 0; j < striker; j++) {
      if (bestPenyerang[j] == -1 || ratings[4][i] > ratings[4][bestPenyerang[j]]) {
        int k;
        for (k = striker - 1; k > j; k--) {
          bestPenyerang[k] = bestPenyerang[k - 1];
        }
        bestPenyerang[j] = i;
        break;
      }
    }
```
Bagian ini akan melalui proses yang sama seperti posisi bek untuk pemain yang berposisi gelandang dan penyerang.

```c
  fprintf(teamFile, "Kiper: %s (%d)\n", playerNames[1][bestKiper], ratings[1][bestKiper]);
  fprintf(teamFile, "Bek:\n");
  for (int i = 0; i < bek; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[2][bestBek[i]], ratings[2][bestBek[i]]);
  }
  fprintf(teamFile, "Gelandang:\n");
  for (int i = 0; i < gelandang; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[3][bestGelandang[i]], ratings[3][bestGelandang[i]]);
  }
  fprintf(teamFile, "Penyerang:\n");
  for (int i = 0; i < striker; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[4][bestPenyerang[i]], ratings[4][bestPenyerang[i]]);
  }
```
Bagian ini adalah untuk mencetak pemain terbaik untuk setiap posisi ke dalam file tim yang telah dibuat sebelumnya. Hal ini dilakukan dengan menggunakan fungsi fprintf(), dimana argumen pertama adalah file yang akan dicetak, dan argumen kedua adalah string yang akan dicetak. %s digunakan untuk mengganti string dengan variabel playerNames yang disimpan dalam array 2 dimensi, sementara %d digunakan untuk mengganti integer dengan variabel ratings yang juga disimpan dalam array 2 dimensi.

```c
  fclose(teamFile);
  free(formasi);
}
```
Setelah mencetak semua pemain terbaik untuk setiap posisi, file tim akan ditutup dengan menggunakan fungsi fclose(). Terakhir, variabel formasi akan di-release dengan menggunakan fungsi free(). Bagian ini sangat penting untuk mencegah kebocoran memori.

https://ibb.co/qFj27wZ

# Soal no. 4

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
```
Library dan header file yang digunakan dalam program, antara lain stdio.h untuk input/output standar, stdlib.h untuk fungsi-fungsi dasar, string.h untuk manipulasi string, unistd.h untuk fungsi-fungsi POSIX, dan time.h untuk mengakses informasi waktu.

```c
int main(int argc, char *argv[]) {
```
Fungsi utama yang akan dieksekusi saat program dijalankan. Fungsi ini menerima dua argumen, yaitu argc (jumlah argumen yang diberikan saat menjalankan program) dan argv (array of strings yang berisi argumen-argumen yang diberikan).

```c
    // Cek jumlah argumen yang diberikan
    if (argc != 5) {
        printf("error: jumlah argumen yang diberikan salah\n");
        return 1;
    }
```
Program melakukan pengecekan jumlah argumen yang diberikan melalui argc. Jika jumlah argumen tidak sama dengan 5, maka program akan mencetak pesan error dan mengembalikan nilai 1, yang menandakan program selesai dengan status error.

```c    
    // Deklarasi variabel-variabel yang dibutuhkan
    int hour, minute, second;
    char *filename;
    struct tm *timeinfo;
    time_t rawtime;
```
Beberapa variabel dideklarasikan untuk menyimpan nilai waktu yang akan digunakan dalam program, seperti hour, minute, second, filename, timeinfo, dan rawtime.

```c
    // Konversi argumen-argumen waktu menjadi integer
    if (strcmp(argv[1], "*") == 0) {
        hour = -1;
    } else {
        hour = atoi(argv[1]);
        if (hour < 0 || hour > 23) {
            printf("error: argumen jam tidak valid\n");
            return 1;
        }
    }

    if (strcmp(argv[2], "*") == 0) {
        minute = -1;
    } else {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            printf("error: argumen menit tidak valid\n");
            return 1;
        }
    }

    if (strcmp(argv[3], "*") == 0) {
        second = -1;
    } else {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            printf("error: argumen detik tidak valid\n");
            return 1;
        }
    }
```
Argumen waktu yang diberikan melalui argv (argumen command line) diubah menjadi integer menggunakan fungsi atoi() dan disimpan dalam variabel hour, minute, dan second. Kemudian, dilakukan validasi untuk memastikan bahwa nilai-nilai tersebut sesuai dengan batasan waktu yang valid.

```c
    // Simpan argumen path file .sh
    filename = argv[4];
```
Argumen path file script shell (.sh) yang akan dijalankan disimpan dalam variabel filename.

```c
    // Cek apakah file .sh yang ditunjuk oleh path tersedia dan bisa diakses
    if (access(filename, F_OK) == -1) {
        printf("error: file %s tidak tersedia atau tidak bisa diakses\n", filename);
        return 1;
    }
```
Dilakukan pengecekan apakah file .sh yang ditunjuk oleh path tersedia dan bisa diakses menggunakan fungsi access(). Jika tidak, maka program mencetak pesan error dan mengembalikan nilai 1.

```c
    // Main loop program
    while (1) {
        // Dapatkan waktu sekarang
        time(&rawtime);
        timeinfo = localtime(&rawtime);
```
Dilakukan loop utama (while(1)) yang akan berjalan selamanya, atau sampai program dihentikan dengan paksa. Dalam loop ini, program akan terus memeriksa waktu sekarang menggunakan fungsi time() dan localtime() untuk memperoleh informasi waktu saat ini dalam bentuk struct tm.

```c
        // Cek apakah waktu sekarang sesuai dengan argumen
        if ((hour == -1 || hour == timeinfo->tm_hour) &&
            (minute == -1 || minute == timeinfo->tm_min) &&
            (second == -1 || second == timeinfo->tm_sec)) {
            // Jika sesuai, jalankan file .sh
            if (fork() == 0) {
                execl("/bin/sh", "sh", filename, NULL);
                exit(0);
            }
        }
```
Informasi waktu saat ini dibandingkan dengan argumen waktu yang diberikan. Jika sesuai, maka program akan melakukan fork() untuk membuat proses baru yang menjalankan file .sh menggunakan fungsi execl(). Proses anak (child process) ini akan menjalankan shell (/bin/sh) dengan argumen file .sh yang akan dijalankan. Setelah itu, proses anak akan keluar (exit(0)).

```c
        // Tunggu 1 detik sebelum cek kembali waktu
        sleep(1);
    }

    return 0;
}
```
Program akan menunggu selama 1 detik (sleep(1)) sebelum memeriksa kembali waktu, dan akan terus berlanjut hingga waktu yang ditentukan tercapai. Jika tidak ada argumen waktu yang cocok, program akan terus berjalan dalam loop utama tanpa melakukan apa-apa. Lalu program akan mengembalikan nilai 0 jika selesai berjalan tanpa ada kesalahan.

Hasil CPU State Minimum:
https://ibb.co/PGchrfj

Hasil Ouput:
https://ibb.co/r56HHB1
