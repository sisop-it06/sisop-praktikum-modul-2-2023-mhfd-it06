#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <time.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>

void removeDir(char *folder_name);
void zip(char *folder_name);
void terminateProgram(char *program_name, int mode, int pid);
void downloadImage(char *folder_name);
void createFolder();
void handleArguments(int argc, char *argv[]);

int main(int argc, char *argv[])
{
    // handleArguments(argc, argv);
    char opt = argv[1][1];
    int mode;
     switch (opt)
        {
        case 'a':
            mode = 1;
            break;
        case 'b':
            mode = 2;
            break;
        }


    pid_t pid = fork();
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }
    if (pid > 0)
    {
        if (mode == 1)
        {
            terminateProgram(argv[0], mode, 0);
        }
        exit(EXIT_SUCCESS);
    }
    umask(0);
    pid_t sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // pid_t parent_id = fork();
    // if (mode == 2) {
    // }

    // if (parent_id == 0) {
    terminateProgram(argv[0], mode, (int)getpid());
    while (1)
    {
        pid_t child_pid = fork();
        if (child_pid < 0)
        {
            perror("Error creating child process");
            exit(EXIT_FAILURE);
        }
        else if (child_pid == 0)
        {
            createFolder();
            exit(EXIT_SUCCESS);
        }
        sleep(30);
    }
    // }
}

void removeDir(char *folder_name)
{
    char *argv[] = {"rm", "-rf", folder_name, NULL};
    execv("/bin/rm", argv);
}

void zip(char *folder_name)
{
    char zip_name[50];
    char cwd[1024];
    char path[100];
    getcwd(cwd, sizeof(cwd));
    sprintf(zip_name, "%s.zip", folder_name);
    sprintf(path, "%s/%s/", cwd, folder_name);
    char *argv[] = {"zip", "-r", zip_name, path, NULL};
    pid_t child_zip, child_rmdir;
    child_zip = fork();
    int status;
    if (child_zip == 0)
    {
        execvp("zip", argv);
    }
    else
    {
        waitpid(child_zip, &status, 0);
        if (WIFEXITED(status))
        {
            child_rmdir = fork();
            if (child_rmdir == 0)
            {
                removeDir(folder_name);
            }
        }
    }
}

void terminateProgram(char *program_name, int mode, int pid)
{
    FILE *killer_file = fopen("killer", "w");
    if (mode == 1)
    {
        fprintf(killer_file, "#!/bin/bash\nkill %d\nrm killer\n", pid);
    }
    else
    {
        fprintf(killer_file, "#!/bin/bash\npkill -f \"%s -b\"\nrm killer\n", program_name);
    }
    fclose(killer_file);
    chmod("killer", 0700);
}

void downloadImage(char *folder_name)
{
    for (int i = 0; i < 15; i++)
    {
        char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
        pid_t pid;
        time_t t, current_time;
        time(&t);
        struct tm *local = localtime(&t);
        strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
        sprintf(fileFormat, "./%s/%s.jpg", folder_name, timeFormat);
        current_time = time(NULL);
        current_time = (current_time % 1000) + 50;
        sprintf(str, "%ld", current_time);
        strcat(URL, str);
        char *argv[] = {"wget", "-O", fileFormat, URL, NULL};
        pid = fork();
        if (pid == 0)
        {
            execvp("wget", argv);
        }
        sleep(5);
    }
    pid_t child_zip;
    child_zip = fork();
    if (child_zip == 0)
    {
        zip(folder_name);
    }
    exit(EXIT_SUCCESS);
}

void createFolder()
{
    time_t t;
    time(&t);
    struct tm *local = localtime(&t);
    char timeFormat[50], URL[50] = "https://picsum.photos/", str[50], fileFormat[50];
    strftime(timeFormat, sizeof(timeFormat), "%Y-%m-%d_%H:%M:%S", local);
    char *argv[] = {"mkdir", "-p", timeFormat, NULL};
    if(fork() == 0) execv("/bin/mkdir", argv);
    downloadImage(timeFormat);
}
