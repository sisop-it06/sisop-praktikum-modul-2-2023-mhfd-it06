#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/wait.h>
#include <fnmatch.h>

#define MANUTD_TEAM "ManUtd"
#define MAX_FILENAME 256

void downloadPlayers();
void extractPlayers();
void filterPlayers();
void categorizePlayers();
void buatTim(int bek, int gelandang, int penyerang);

int main() {
    downloadPlayers();
    extractPlayers();
    filterPlayers();
    categorizePlayers();
    
    int bek, gelandang, penyerang;
    printf("Masukkan jumlah bek: ");
    scanf("%d", &bek);
    printf("Masukkan jumlah gelandang: ");
    scanf("%d", &gelandang);
    printf("Masukkan jumlah penyerang: ");
    scanf("%d", &penyerang);
  
    buatTim(bek, gelandang, penyerang);
    return 0;
}

void downloadPlayers() {
    char* args[] = {"curl", "-L", "-o", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error downloading players\n");
            exit(EXIT_FAILURE);
        }
    }
}

void extractPlayers() {
    char* args[] = {"unzip", "players.zip", NULL};
    pid_t pid = fork();

    if (pid == -1) {
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { // child process
        execvp(args[0], args);
        perror("execvp");
        exit(EXIT_FAILURE);
    } else { // parent process
        int status;
        waitpid(pid, &status, 0);
        if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
            printf("Error extracting players\n");
            exit(EXIT_FAILURE);
        }
    }

    if (remove("players.zip") != 0) {
        printf("Error deleting file: players.zip\n");
    }
}

void filterPlayers() {
    DIR* dir = opendir("players");
    struct dirent* entry;
    char fileName[MAX_FILENAME];

    if (dir == NULL) {
        printf("Error opening directory\n");
        exit(EXIT_FAILURE);
    }

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_REG) { // jika entri adalah file biasa
            if (fnmatch("*_ManUtd_*_*.png", entry->d_name, 0) != 0) {
                sprintf(fileName, "%s/%s", "players", entry->d_name);
                if (unlink(fileName) != 0) {
                    printf("Error deleting file: %s\n", fileName);
                }
            }
        }
    }

    closedir(dir);
}

void categorizePlayers() {
    DIR * dir = opendir("./players");
    struct dirent * entry;
    char * image_file;
    char * positions[] = {
      "Kiper",
      "Bek",
      "Gelandang",
      "Penyerang"
    };

    if (dir == NULL) {
      printf("Error opening directory\n");
      exit(EXIT_FAILURE);
    }

    // buat direktori baru
    int i;
    for (i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) {
        char dir_name[MAX_FILENAME];
        sprintf(dir_name, "./%s", positions[i]);

        if (access(dir_name, F_OK) != 0) {
            char *args[] = {
                "mkdir",
                dir_name,
                NULL
            };
            pid_t pid = fork();

            if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
            } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
            } else {
                int status;
                waitpid(pid, &status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                    printf("Error creating directory: %s\n", dir_name);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }

    rewinddir(dir);

    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        image_file = entry -> d_name;
        char * ext = strrchr(image_file, '.');

        if (ext != NULL && (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0 || strcmp(ext, ".png") == 0)) {
          int i;
          for (i = 0; i < sizeof(positions) / sizeof(positions[0]); i++) {
            if (strstr(image_file, positions[i]) != NULL) {
              char new_path[MAX_FILENAME];
              sprintf(new_path, "./%s/%s", positions[i], image_file);

              char old_path[MAX_FILENAME];
              sprintf(old_path, "./players/%s", image_file);

              char * args[] = {
                "mv",
                old_path,
                new_path,
                NULL
              };
              pid_t pid = fork();

              if (pid == -1) {
                perror("fork");
                exit(EXIT_FAILURE);
              } else if (pid == 0) {
                execvp(args[0], args);
                perror("execvp");
                exit(EXIT_FAILURE);
              } else {
                int status;
                waitpid(pid, & status, 0);
                if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
                  printf("Error moving file: %s\n", image_file);
                  exit(EXIT_FAILURE);
                }
              }
              break;
            }
          }
        }
      }
    }

    closedir(dir);

    char * args[] = {
      "rm",
      "-r",
      "./players",
      NULL
    };
    pid_t pid = fork();

    if (pid == -1) {
      perror("fork");
      exit(EXIT_FAILURE);
    } else if (pid == 0) {
      execvp(args[0], args);
      perror("execvp");
      exit(EXIT_FAILURE);
    } else {
      int status;
      waitpid(pid, & status, 0);
      if (WIFEXITED(status) && WEXITSTATUS(status) != 0) {
        printf("Error removing directory: %s\n", "./players");
        exit(EXIT_FAILURE);
      }
    }
}

void buatTim(int bek, int gelandang, int penyerang) {
  char * positions[] = {
    "Kiper",
    "Bek",
    "Gelandang",
    "Penyerang"
  };
  char * formasi = malloc(sizeof(char) * MAX_FILENAME);
  // sprintf(formasi, "/home/kali/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang); // Kalo mau naro di home/[users]
  sprintf(formasi, "Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE * teamFile = fopen(formasi, "w");
  if (teamFile == NULL) {
    printf("Error creating team file: %s\n", strerror(errno));
    return;
  }
  fprintf(teamFile, "Formasi %d-%d-%d\n\n", bek, gelandang, penyerang);

  int ratings[5][20] = {
    0
  }; // Array untuk menyimpan rating semua pemain
  char playerNames[5][20][MAX_FILENAME]; // Array untuk menyimpan nama semua pemain
  int count[5] = {
    0
  }; // Array untuk menyimpan jumlah pemain di setiap posisi

  // Buka setiap folder posisi dan dapatkan rating dan nama setiap pemain
  for (int i = 0; i < 4; i++) {
    DIR * dir = opendir(positions[i]);
    if (dir == NULL) {
      printf("Error opening directory %s\n", positions[i]);
      continue;
    }
    struct dirent * entry;
    char fileName[MAX_FILENAME];
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG) { // jika entri adalah file biasa
        sprintf(fileName, "%s/%s", positions[i], entry -> d_name);
        char * rating = strrchr(entry -> d_name, '_') + 1; // dapatkan rating dari nama file
        char * name = entry -> d_name;
        name[strlen(name) - strlen(rating) - 1] = '\0'; // hapus peringkat dari nama file
        int ratingVal = atoi(rating);
        ratings[i + 1][count[i + 1]] = ratingVal;
        strcpy(playerNames[i + 1][count[i + 1]], name); // copy nama pemain ke array
        count[i + 1]++;
      }
    }
    closedir(dir);
  }

  // Dapatkan pemain terbaik untuk setiap posisi berdasarkan rating mereka
  int bestKiper = 0;
  int bestBek[bek];
  int bestGelandang[gelandang];
  int bestPenyerang[penyerang];

  for (int i = 0; i < count[1]; i++) {
    if (ratings[1][i] > ratings[1][bestKiper]) {
      bestKiper = i;
    }
  }

  for (int i = 0; i < bek; i++) {
    bestBek[i] = -1;
  }

  for (int i = 0; i < count[2]; i++) {
    for (int j = 0; j < bek; j++) {
      if (bestBek[j] == -1 || ratings[2][i] > ratings[2][bestBek[j]]) {
        int k;
        for (k = bek - 1; k > j; k--) {
          bestBek[k] = bestBek[k - 1];
        }
        bestBek[j] = i;
        break;
      }
    }
  }

  for (int i = 0; i < gelandang; i++) {
    bestGelandang[i] = -1;
  }

  for (int i = 0; i < count[3]; i++) {
    for (int j = 0; j < gelandang; j++) {
      if (bestGelandang[j] == -1 || ratings[3][i] > ratings[3][bestGelandang[j]]) {
        int k;
        for (k = gelandang - 1; k > j; k--) {
          bestGelandang[k] = bestGelandang[k - 1];
        }
        bestGelandang[j] = i;
        break;
      }
    }
  }

  for (int i = 0; i < penyerang; i++) {
    bestPenyerang[i] = -1;
  }

  for (int i = 0; i < count[4]; i++) {
    for (int j = 0; j < penyerang; j++) {
      if (bestPenyerang[j] == -1 || ratings[4][i] > ratings[4][bestPenyerang[j]]) {
        int k;
        for (k = penyerang - 1; k > j; k--) {
          bestPenyerang[k] = bestPenyerang[k - 1];
        }
        bestPenyerang[j] = i;
        break;
      }
    }
  }

  // Print pemain terbaik untuk setiap posisi ke file tim
  fprintf(teamFile, "Kiper: %s (%d)\n", playerNames[1][bestKiper], ratings[1][bestKiper]);
  fprintf(teamFile, "Bek:\n");
  for (int i = 0; i < bek; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[2][bestBek[i]], ratings[2][bestBek[i]]);
  }
  fprintf(teamFile, "Gelandang:\n");
  for (int i = 0; i < gelandang; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[3][bestGelandang[i]], ratings[3][bestGelandang[i]]);
  }
  fprintf(teamFile, "Penyerang:\n");
  for (int i = 0; i < penyerang; i++) {
    fprintf(teamFile, "- %s (%d)\n", playerNames[4][bestPenyerang[i]], ratings[4][bestPenyerang[i]]);
  }

  fclose(teamFile);
  free(formasi);
}