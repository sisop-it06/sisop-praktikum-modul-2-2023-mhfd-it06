#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zip.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

int zip(char *zipfilename, char *directory)
{
    struct zip *za = zip_open(zipfilename, ZIP_CREATE, NULL);
    if (!za)
    {
        return 0;
    }

    DIR *d = opendir(directory);
    if (!d)
    {
        zip_close(za);
        return 0;
    }

    struct dirent *dir;
    while ((dir = readdir(d)) != NULL)
    {
        const char *ext = strrchr(dir->d_name, '.');
        if (ext && (strcmp(ext, ".jpg") == 0 || strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".png") == 0))
        {
            char filepath[256];
            snprintf(filepath, sizeof(filepath), "%s/%s", directory, dir->d_name);

            struct zip_source *source = zip_source_file(za, filepath, 0, 0);
            if (!source)
            {
                zip_close(za);
                closedir(d);
                return 0;
            }

            if (zip_file_add(za, dir->d_name, source, ZIP_FL_OVERWRITE) < 0)
            {
                zip_source_free(source);
                zip_close(za);
                closedir(d);
                return 0;
            }
        }
    }
    closedir(d);
    return zip_close(za) == 0 ? 1 : 0;
}

int main()
{
    // Download file zip
    system("wget \"https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq\" -O images.zip");

    // Extract file zip
    const char *filename = "images.zip";
    struct zip *zip_file = zip_open(filename, 0, NULL);
    if (!zip_file)
    {
        fprintf(stderr, "Error opening zip file.\n");
        return 1;
    }

    int num_entries = zip_get_num_entries(zip_file, 0);
    for (int i = 0; i < num_entries; i++)
    {
        struct zip_stat file_stat;
        if (zip_stat_index(zip_file, i, 0, &file_stat) != 0)
        {
            fprintf(stderr, "Error getting file stats from zip.\n");
            zip_close(zip_file);
            return 1;
        }

        char output_file[256];
        strcpy(output_file, file_stat.name);

        if (output_file[strlen(output_file) - 1] == '/')
        {
            continue;
        }

        struct zip_file *file = zip_fopen_index(zip_file, i, 0);
        if (!file)
        {
            fprintf(stderr, "Error opening file inside zip.\n");
            zip_close(zip_file);
            return 1;
        }

        FILE *fp = fopen(output_file, "wb");
        if (!fp)
        {
            fprintf(stderr, "Error creating output file.\n");
            zip_fclose(file);
            zip_close(zip_file);
            return 1;
        }

        char buffer[4096];
        int n;
        do
        {
            n = zip_fread(file, buffer, sizeof(buffer));
            if (n > 0)
            {
                fwrite(buffer, n, 1, fp);
            }
        } while (n > 0);

        fclose(fp);
        zip_fclose(file);
    }
    
    zip_close(zip_file);

    const char *valid_extensions[] = {".jpg", ".jpeg"};
    int num_valid_extensions = sizeof(valid_extensions) / sizeof(char*);

    DIR *dir = opendir(".");
    if (dir == NULL) {
        fprintf(stderr, "Error\n");
        return 1;
    }

    const int MAX_FILES = 1000;
    char filenames[MAX_FILES][256];
    int num_files = 0;
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL) {
        char *ext = strrchr(entry->d_name, '.');
        if (ext == NULL) {
            continue;
        }
        for (int i = 0; i < num_valid_extensions; i++) {
            if (strcmp(ext, valid_extensions[i]) == 0) {
                strcpy(filenames[num_files], entry->d_name);
                num_files++;
                break;
            }
        }
    }
    closedir(dir);

    srand(time(NULL));
    int random_index = rand() % num_files;
    char *random_filename = filenames[random_index];
    printf("Shift Penjagaan: %s\n", random_filename);

    printf("File berhasil di-un zip.\n");
    // Buat direktori HewanDarat, HewanAmphibi, dan HewanAir jika belum ada
    system("mkdir -p HewanDarat");
    system("mkdir -p HewanAmphibi");
    system("mkdir -p HewanAir");

    // Pindahkan file gambar ke direktori yang sesuai
    system("mv *_darat.jpg HewanDarat");
    system("mv *_amphibi.jpg HewanAmphibi");
    system("mv *_air.jpg HewanAir");

    // Zip file di dalam direktori HewanDarat, HewanAmphibi, dan HewanAir
    zip("HewanDarat.zip", "HewanDarat");
    zip("HewanAmphibi.zip", "HewanAmphibi");
    zip("HewanAir.zip", "HewanAir");

    printf("Pemindahan file gambar berhasil dilakukan.\n");

    return 0;
}
